# Shelf
This is a place to collect and store documents.

These documents are open to the public.

The primary intent of this repository is to collect data, not leak. Sites like [MormonLeaks.io](https://mormonleaks.io/) are better suited to handle sensitive data.